<?php

/**
 * @file
 * Provides field widget hooks for bmap module.
 */

/**
 * Implements hook_field_widget_info().
 *
 * Manage fields -> Widget
 *
 * @author cliu
 */
function bmap_field_widget_info() {
  return array(
    /*
    'amap' => array(
      'label'       => t('Aliyun Map'),
      'field types' => array('bmap'),
    ),
    */
    'bmap' => array(
      'label'       => t('Baidu Map'),
      'field types' => array('bmap'),
    ),
    /*
    'gmap' => array(
      'label'       => t('Google Map'),
      'field types' => array('bmap'),
    ),
    */
  );
}

/**
 * Implements hook_field_widget_form().
 *
 * 这是在节点编辑页面需要显示的东西。
 */
function bmap_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = isset($items[$delta]['value']) ? $items[$delta]['value'] : '';
  // Substitute the decimal separator.
  if ($field['type'] == 'bmap') {
    // $value = strtr($value, '.', $field['settings']['decimal_separator']);
  }

  if (!empty($value) && is_string($value)) {
    $bmap_settings = unserialize($value);
  }
  else {
    $bmap_settings = array();
  }
  // TODO: 在编辑页面做地图的实时显示功能。
  // print_r($value); exit;

  $element += array(
    '#type'           => 'fieldset',
    '#title'          => '地图设定',
    '#collapsible'    => TRUE,
    '#collapsed'      => FALSE,
    // '#default_value' => $value,
    // Allow a slightly larger size that the field length to allow for some
    // configurations where all characters won't fit in input field.
    // '#size' => $field['type'] == 'number_decimal' ? $field['settings']['precision'] + 4 : 12,
    // Allow two extra characters for signed values and decimal separator.
    // '#maxlength' => $field['type'] == 'number_decimal' ? $field['settings']['precision'] + 2 : 10,
    // Extract the number type from the field type name for easier validation.
    // '#number_type' => str_replace('number_', '', $field['type']),
  );

  $element['bmap_width'] = array(
    '#type'           => 'textfield',
    '#title'          => t('地图宽度'),
    '#required'       => TRUE,
    '#default_value'  => isset($bmap_settings['bmap_width']) ? $bmap_settings['bmap_width'] : $instance['settings']['bmap_width'],
    '#description'    => t('地图字段的显示宽度，譬如“400px”。'),
  );
  $element['bmap_height'] = array(
    '#type'           => 'textfield',
    '#title'          => t('地图高度'),
    '#required'       => TRUE,
    '#default_value'  => isset($bmap_settings['bmap_height']) ? $bmap_settings['bmap_height'] : $instance['settings']['bmap_height'],
    '#description'    => t('地图字段的显示高度，譬如“240px”。'),
  );
  $element['bmap_default_center'] = array(
    '#type'           => 'fieldset',
    '#title'          => '中心点坐标',
    '#collapsible'    => TRUE,
    '#collapsed'      => FALSE,
  );
  $element['bmap_default_center']['longitude'] = array(
    '#type'           => 'textfield',
    '#title'          => '中心点经度',
    '#required'       => TRUE,
    '#description'    => '以小数的方式显示，正确的取值范围是-180到180',
    '#default_value'  => isset($bmap_settings['bmap_center_longitude']) ? $bmap_settings['bmap_center_longitude'] : $instance['settings']['bmap_default_center_longitude'],
  );
  $element['bmap_default_center']['latitude'] = array(
    '#type'           => 'textfield',
    '#title'          => '中心点纬度',
    '#required'       => TRUE,
    '#description'    => '以小数的方式显示，正确的取值范围是-90到90',
    '#default_value'  => isset($bmap_settings['bmap_center_latitude']) ? $bmap_settings['bmap_center_latitude'] : $instance['settings']['bmap_default_center_latitude'],
  );
  $zoom_options = array(
    '3'  => '3',
    '4'  => '4',
    '5'  => '5',
    '6'  => '6',
    '7'  => '7',
    '8'  => '8',
    '9'  => '9',
    '10' => '10',
    '11' => '11',
    '12' => '12',
    '13' => '13',
    '14' => '14',
    '15' => '15',
    '16' => '16',
    '17' => '17',
    '18' => '18',
  );
  $element['bmap_zoom'] = array(
    '#type'           => 'select',
    '#title'          => t('缩放比例'),
    '#options'        => $zoom_options,
    '#default_value'  => isset($bmap_settings['bmap_zoom']) ? $bmap_settings['bmap_zoom'] : $instance['settings']['bmap_zoom'],
    '#description'    => t('使用的比例级别总共有3-18级共16个级别：4级全国，8级省，13级市，16级街道。'),
  );

  return array('value' => $element);
}