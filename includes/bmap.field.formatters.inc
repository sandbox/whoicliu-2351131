<?php

/**
 * @file
 * Drupal field formatter hooks and helper functions.
 */

/**
 * Implements hook_field_formatter_info().
 *
 * 字段显示方式的设定。这里有三个显示方式:
 *   1. 默认的地图显示
 *   2. 卫星地图的显示
 *   3. 三维地图的显示
 *
 * @retrun array
 * @author cliu
 */
function bmap_field_formatter_info() {
  $formatters = array(
    'bmap_default_formatter' => array(
      'label' => t('Default'),
      'field types' => array('bmap'),
      'settings' => array(
        'Basic'  => array(
          'BaiduMapMouseWheelControl'       => 1,
          'BaiduMapLargeControl'            => 1,
          'BaiduMapScaleControl'            => 1,
          'BaiduMapOverviewControl'         => 0,
          'BaiduMapAnchor'                  => 0,
        ),
        'BusAPI'  => array(
          'BaiduMapBusTransfer'             => 0,
          'BaiduMapBusLine'                 => 0,
          'BaiduMapBusStation'              => 0,
        ),
        'Cube'  => array(
          'cubeControl'                   => 0,
        ),
      ),
      'description' => t('地图默认显示方式的特性设置。'),
    ),
    'satellite_map' => array(
      'label' => t('卫星地图'),
      'field types' => array('bmap'),
    ),
    '3d_map' => array(
      'label' => t('3D地图'),
      'field types' => array('bmap'),
    ),  
  );

  return $formatters;
}

/**
 * Implements hook_field_formatter_settings_form().
 *
 * 地图显示方式的表单设置。
 *
 * @param $field       字段结构
 * @param $instance    在某实体中字段的实例化结构
 * @param $view_mode   查看方式
 * @param $form        实体配置表单数组
 * @param $form_state  实体配置表单的表单状态
 *
 * @retrun array       格式设置表单数组
 * @author cliu
 */
function bmap_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display      = $instance['display'][$view_mode];
  $settings     = $display['settings'];
  $display_type = $display['type'];
  $bmap_type    = $instance['widget']['type'];

  $element = array();

  // print_r($bmap_type); exit;
  if ($display_type == 'bmap_default_formatter') {
    $element['Basic'] = array(
      '#type'          => 'fieldset',
      '#title'         => t('基础属性设置'),
      '#tree'          => TRUE,
      '#collapsible'   => TRUE,
      '#collapsed'     => FALSE,
    );
    if ($bmap_type == 'bmap') {
      // 百度地图的显示特征设置
      $element['Basic']['BaiduMapMouseWheelControl'] = array(
        '#type'           => 'checkbox',
        '#title'          => t('启用鼠标滚轮缩放地图'),
        '#default_value'  => $settings['Basic']['BaiduMapMouseWheelControl'],
        /*
        '#states' => array(
          'visible' => array(
            ':input[name$="[settings_edit_form][settings][slideshow_controls]"]' => array('checked' => TRUE),
          ),
        ),
        */
        // '#description'    => t('通过鼠标滚轮来缩放地图。'),
      );
      $element['Basic']['BaiduMapLargeControl'] = array(
        '#type'           => 'checkbox',
        '#title'          => t('启用缩放控件'),
        '#default_value'  => $settings['Basic']['BaiduMapLargeControl'],
      );
      $element['Basic']['BaiduMapScaleControl'] = array(
        '#type'           => 'checkbox',
        '#title'          => t('启用比例尺控件'),
        '#default_value'  => $settings['Basic']['BaiduMapScaleControl'],
      );
      $element['Basic']['BaiduMapOverviewControl'] = array(
        '#type'           => 'checkbox',
        '#title'          => t('启用鹰眼控件'),
        '#default_value'  => $settings['Basic']['BaiduMapOverviewControl'],
      );
      $element['Basic']['BaiduMapAnchor'] = array(
        '#type'           => 'checkbox',
        '#title'          => t('启用可点击功能的图层'),
        '#default_value'  => $settings['Basic']['BaiduMapAnchor'],
      );
      $element['Basic']['BaiduMapTypeControl'] = array(
        '#type'           => 'checkbox',
        '#title'          => t('启用地图类型控件'),
        '#default_value'  => $settings['Basic']['BaiduMapTypeControl'],
      );
      $element['Basic']['BaiduMapTrafficLayer'] = array(
        '#type'           => 'checkbox',
        '#title'          => t('启用交通流量图层'),
        '#default_value'  => $settings['Basic']['BaiduMapTrafficLayer'],
      );
      
    }
    /**
    $element['BusAPI'] = array(
      '#type'          => 'fieldset',
      '#title'         => t('公交服务API'),
      '#tree'          => TRUE,
      '#collapsible'   => TRUE,
      '#collapsed'     => TRUE,
    );

    $element['RegionAPI'] = array(
      '#type'          => 'fieldset',
      '#title'         => t('行政区划API'),
      '#tree'          => TRUE,
      '#collapsible'   => TRUE,
      '#collapsed'     => TRUE,
    );
    */
    $element['BusAPI'] = array(
      '#type'          => 'fieldset',
      '#title'         => t('公交属性设置'),
      '#tree'          => TRUE,
      '#collapsible'   => TRUE,
      '#collapsed'     => TRUE,
    );
    $element['BusAPI']['BaiduMapBusTransfer'] = array(
      '#type'           => 'checkbox',
      '#title'          => t('公交换乘'),
      '#default_value'  => $settings['BusAPI']['BaiduMapBusTransfer'],
    );
    $element['BusAPI']['BaiduMapBusLine'] = array(
      '#type'           => 'checkbox',
      '#title'          => t('线路查询'),
      '#default_value'  => $settings['BusAPI']['BaiduMapBusLine'],
    );
    $element['BusAPI']['BaiduMapBusStation'] = array(
      '#type'           => 'checkbox',
      '#title'          => t('站点查询'),
      '#default_value'  => $settings['BusAPI']['BaiduMapBusStation'],
    );

    $element['Cube'] = array(
      '#type'          => 'fieldset',
      '#title'         => t('地图魔方API'),
      '#tree'          => TRUE,
      '#collapsible'   => TRUE,
      '#collapsed'     => TRUE,
    );
    $element['Cube']['cubeControl'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('启用地图模仿'),
      '#default_value' => $settings['Cube']['cubeControl'],
    );
  }

  if ($display_type == 'satellite_map') {
    // TODO: 卫星地图的处理模式。
    $element['bmap_width'] = array(
      '#type'           => 'textfield',
      '#title'          => t('地图宽度'),
      '#required'       => TRUE,
      '#default_value'  => $settings['bmap_width'],
      '#description'    => t('地图字段的显示宽度，譬如“400px”。'),
    );
    $element['bmap_height'] = array(
      '#type'           => 'textfield',
      '#title'          => t('地图高度'),
      '#required'       => TRUE,
      '#default_value'  => $settings['bmap_height'],
      '#description'    => t('地图字段的显示高度，譬如“300px”。'),
    );
  }

  if ($display_type == '3d_map') {
    // TODO: 3D地图显示的处理模式。
    $element['bmap_width'] = array(
      '#type'           => 'textfield',
      '#title'          => t('地图宽度'),
      '#required'       => TRUE,
      '#default_value'  => $settings['bmap_width'],
      '#description'    => t('地图字段的显示宽度，譬如“400px”。'),
    );
    $element['bmap_height'] = array(
      '#type'           => 'textfield',
      '#title'          => t('地图高度'),
      '#required'       => TRUE,
      '#default_value'  => $settings['bmap_height'],
      '#description'    => t('地图字段的显示高度，譬如“300px”。'),
    );
  }
  
  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 *
 * @param $field       字段结构
 * @param $instance    在某实体中字段的实例化结构
 * @param $view_mode   查看方式
 *
 * @author cliu
 * @return $element    摘要显示方式。
 */
function bmap_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  // print_r($settings); exit;
  $summary = array();
  if ($display['type'] == 'bmap_default_formatter') {
    $summary[] = t('地图默认显示方式');
    if (isset($settings['Basic']['BaiduMapMouseWheelControl']) && $settings['Basic']['BaiduMapMouseWheelControl']) {
      $summary[] = t('鼠标滚动缩放地图启用');
    }
    if (isset($settings['Basic']['BaiduMapLargeControl']) && $settings['Basic']['BaiduMapLargeControl']) {
      $summary[] = t('地图缩放控件启用');
    }
    if (isset($settings['Basic']['BaiduMapScaleControl']) && $settings['Basic']['BaiduMapScaleControl']) {
      $summary[] = t('比例尺控件启用');
    }
    if (isset($settings['Basic']['BaiduMapOverviewControl']) && $settings['Basic']['BaiduMapOverviewControl']) {
      $summary[] = t('鹰眼控件启用');
    }
    if (isset($settings['Basic']['BaiduMapAnchor']) && $settings['Basic']['BaiduMapAnchor']) {
      $summary[] = t('可点击图层启用');
    }

    if (isset($settings['BusAPI']['BaiduMapBusTransfer']) && $settings['BusAPI']['BaiduMapBusTransfer']) {
      $summary[] = t('公交换乘服务启用');
    }
    if (isset($settings['BusAPI']['BaiduMapBusLine']) && $settings['BusAPI']['BaiduMapBusLine']) {
      $summary[] = t('公交线路查询启用');
    }
    if (isset($settings['BusAPI']['BaiduMapBusStation']) && $settings['BusAPI']['BaiduMapBusStation']) {
      $summary[] = t('公交站点查询启用');
    }

    


    if (isset($settings['Cube']['cubeControl']) && $settings['Cube']['cubeControl']) {
      $summary[] = t('地图魔方启用');
    }
    
  }

  if ($display['type'] == 'satellite_map') {
    $summary[] = t('卫星地图显示');
  }

  if ($display['type'] == '3d_map') {
    $summary[] = t('三维地图显示');
  }

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 *
 * Return data for display when rendered according to different view formatters.
 *
 * @param $entity_type 实体类型，例如node,taxonomy,block,user等。
 * @param $entity      操作类型，一般为内容类型的机器名。
 * @param $field       字段结构
 * @param $instance    在某实体中字段的实例化结构
 * @param $langcode    跟$items关联的语言代码
 * @param $items       $entity->{$field['field_name']}[$langcode]，未设置默认为空
 * @param $display     显示设置，一般包含两个参数，type：formatter名称；settings：formatter设置数组
 *
 * @author cliu
 * @return $element    显示方式查看数组。
 */
function bmap_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $output = '';
  $settings = $display['settings'];
  // print_r($settings); exit;
  switch ($display['type']) {
    case 'bmap_default_formatter':
      // print_r($items); exit;
      foreach ($items as $delta => $item) {
        // $output = number_format($item['value'], $settings['scale'], $settings['decimal_separator'], $settings['thousand_separator']);

        drupal_add_js(drupal_get_path('module', 'bmap') .'/js/script.js');
        // drupal_add_css(drupal_get_path('module', 'bmap') .'/bmap_type/amap/map.js');
        // print_r(unserialize($item['value'])); exit;

        $bmap_settings = unserialize($item['value']);
        $output .= '<script type="text/javascript" src="http://api.map.baidu.com/api?v=' . BMAP_API_VERSION . '&ak=' . variable_get('bmap_api_key') . '"></script>';
        $output .= '<div id="map_wrapper">';
        $output .= '<div class="map_container" id="' . $bmap_settings['bmap_id'] . '" style="width:' . $bmap_settings['bmap_width'] . ';height:' . $bmap_settings['bmap_height'] . ';"></div></div>
      <script language="javascript">
          var map=new BMap.Map("' . $bmap_settings['bmap_id'] . '");
          map.centerAndZoom(new BMap.Point(' . $bmap_settings['bmap_center_longitude'] . ',' . $bmap_settings['bmap_center_latitude'] . '),' . $bmap_settings['bmap_zoom'] . ');';
        $output .= "\n";
        // 通过鼠标滚轮缩放地图
        if (isset($settings['Basic']['BaiduMapMouseWheelControl']) && $settings['Basic']['BaiduMapMouseWheelControl']) {
          $output .= file_get_contents(drupal_get_path('module', 'bmap') .'/js/features/BaiduMapMouseWheelControl.js');
        }

        // 添加缩放控件
        if (isset($settings['Basic']['BaiduMapLargeControl']) && $settings['Basic']['BaiduMapLargeControl']) {
          $output .= file_get_contents(drupal_get_path('module', 'bmap') .'/js/features/BaiduMapLargeControl.js');
        }

        // 添加比例尺控件
        if (isset($settings['Basic']['BaiduMapScaleControl']) && $settings['Basic']['BaiduMapScaleControl']) {
          $output .= file_get_contents(drupal_get_path('module', 'bmap') .'/js/features/BaiduMapScaleControl.js');
        }

        // 添加鹰眼控件
        if (isset($settings['Basic']['BaiduMapOverviewControl']) && $settings['Basic']['BaiduMapOverviewControl']) {
          $output .= file_get_contents(drupal_get_path('module', 'bmap') .'/js/features/BaiduMapOverviewControl.js');
        }
        
        // 地图类型控件，默认位于地图右上方。
        if (isset($settings['Basic']['BaiduMapTypeControl']) && $settings['Basic']['BaiduMapTypeControl']) {
          $output .= file_get_contents(drupal_get_path('module', 'bmap') .'/js/features/BaiduMapTypeControl.js');
        }
        
        // 交通流量图层
        if (isset($settings['Basic']['BaiduMapTrafficLayer']) && $settings['Basic']['BaiduMapTrafficLayer']) {
          $output .= file_get_contents(drupal_get_path('module', 'bmap') .'/js/features/BaiduMapTrafficLayer.js');
        }

        // 添加可点击图层 TODO
        if (isset($settings['Basic']['BaiduMapAnchor']) && $settings['Basic']['BaiduMapAnchor']) {
          $output .= file_get_contents(drupal_get_path('module', 'bmap') .'/js/features/BaiduMapAnchor.js');
        }

        // 公交换乘功能 TODO
        if (isset($settings['BusAPI']['BaiduMapBusTransfer']) && $settings['BusAPI']['BaiduMapBusTransfer']) {
          $output .= file_get_contents(drupal_get_path('module', 'bmap') .'/js/features/BaiduMapBusTransfer.js');
        }

        // 公交线路查询 TODO
        if (isset($settings['BusAPI']['BaiduMapBusLine']) && $settings['BusAPI']['BaiduMapBusLine']) {
          // $output .= file_get_contents(drupal_get_path('module', 'bmap') .'/js/features/BaiduMapBusLine.js');
        }

        // 公交站点查询 TODO
        if (isset($settings['BusAPI']['BaiduMapBusStation']) && $settings['BusAPI']['BaiduMapBusStation']) {
          // $output .= file_get_contents(drupal_get_path('module', 'bmap') .'/js/features/BaiduMapBusStation.js');
        }

        
        // 启用地图魔方
        if (isset($settings['Cube']['cubeControl']) && $settings['Cube']['cubeControl']) {
          // $settings['cubeControl'] = $settings['Cube']['cubeControl'];
          $output .= file_get_contents(drupal_get_path('module', 'bmap') .'/js/features/cubeControl.js');
        }

        // print_r($settings['BaiduMapMouseWheelControl']); exit;
        // $output .= 'map.openInfoWindow(new AliLatLng(36.135332,120.4235),"标记你的位置","您当前的位置是: 山东省 > 青岛市<br /><a href=\'http://www.aliyun.com\'>阿里云</a>");';
        $output .= '</script>';
        $element[$delta] = array('#markup' => $output);
      }
      break;
    case 'satellite_map':
      foreach ($items as $delta => $item) {
        $element[$delta] = array('#markup' => $item['value']);
      }   
      break;
    case '3d_map':
      foreach ($items as $delta => $item) {
        $element[$delta] = array('#markup' => $item['value']);
      }   
      break;
  }

  return $element;
}