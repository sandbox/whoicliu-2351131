<?php

/**
 * @file
 * Drupal bmap block hooks and helper functions.
 */

 /**
 * Implements hook_block_info().
 *
 * 生成地图工具区块
 */
function bmap_block_info() {
  $blocks['bmap_tools'] = array(
    // info: The name of the block.
    'info' => t('地图工具区块'),
    // Block caching options (per role, per user, etc.)
    'cache' => DRUPAL_CACHE_PER_ROLE, // default
    'status' => TRUE,
    'region' => 'sidebar_first',  // Not usually provided.
  );

  /*
  $blocks['example_empty'] = array(
    'info' => t('Example: empty block'),
    'status' => TRUE,
    'region' => 'sidebar_first',  // Not usually provided.
    'visibility' => BLOCK_VISIBILITY_LISTED,  // Not usually provided.
    'pages' => 'node/*', // Not usually provided here.
  );
  */

  return $blocks;
}

/**
 * Implements hook_block_configure().
 *
 * This hook declares configuration options for blocks provided by this module.
 */
function bmap_block_configure($delta = '') {
  $form = array();
  if ($delta == 'bmap_tools') {
    $form['bmap_block_tools_string'] = array(
      '#type' => 'textfield',
      '#title' => t('Block contents'),
      '#size' => 60,
      '#description' => t('This text will appear in the example block.'),
      '#default_value' => variable_get('bmap_block_tools_string',  t('地图提示信息。')),
    );
  }
  return $form;
}

/**
 * Implements hook_block_save().
 *
 * This hook declares how the configured options for a block
 * provided by this module are saved.
 */
function bmap_block_save($delta = '', $edit = array()) {
  if ($delta == 'bmap_tools') {
    // Have Drupal save the string to the database.
    variable_set('bmap_block_tools_string', $edit['bmap_block_tools_string']);
  }
  return;
}

/**
 * Implements hook_block_view().
 *
 * This hook generates the contents of the blocks themselves.
 */
function bmap_block_view($delta = '') {
  //The $delta parameter tells us which block is being requested.
  $field_bmap = field_info_instance('node', 'field_bmap', 'bmap');
  $bmap_field_settings = $field_bmap['display']['default']['settings'];
  // print_r($field_bmap['display']['default']['settings']); exit;
  switch ($delta) {
    case 'bmap_tools':
      $block['subject'] = t('地图工具');
      $block['content'] = bmap_bus($delta,$bmap_field_settings) . '<div id="resultDiv">' . bmap_block_tools_contents($delta) . '</div>';
      break;
    /**
    case 'example_empty':
      $block['subject'] = t('Title of second block (example_empty)');
      $block['content'] = block_example_contents($delta);
      break;
    case 'example_uppercase':
      $block['subject'] = t("uppercase this please");
      $block['content'] = t("This block's title will be changed to uppercase. Any other block with 'uppercase' in the subject or title will also be altered. If you change this block's title through the UI to omit the word 'uppercase', it will still be altered to uppercase as the subject key has not been changed.");
      break;
    */
  }
  return $block;
}

/**
 * A module-defined block content function.
 */
function bmap_block_tools_contents($which_block) {
  switch ($which_block) {
    case 'bmap_tools':
      return variable_get('bmap_block_tools_string');
    /**
    case 'example_empty':
      return;
    // */
  }
}

/**
 * 公交服务
 */
function bmap_bus($which_block,$settings) {
  $output = '';
  switch ($which_block) {
    case 'bmap_tools':

      if (isset($settings['BusAPI']['AliMapBusTransfer']) && $settings['BusAPI']['AliMapBusTransfer']) {
        $bmap_bus_transfer_form = drupal_get_form('bmap_bus_transfer');
        $output .= drupal_render($bmap_bus_transfer_form);
      }
      if (isset($settings['BusAPI']['AliMapBusLine']) && $settings['BusAPI']['AliMapBusLine']) {
        $bmap_bus_line_form = drupal_get_form('bmap_bus_line');
        $output .= drupal_render($bmap_bus_line_form);
      }
      if (isset($settings['BusAPI']['AliMapBusStation']) && $settings['BusAPI']['AliMapBusStation']) {
        $bmap_bus_station_form = drupal_get_form('bmap_bus_station');
        $output .= drupal_render($bmap_bus_station_form);
      }
      return $output;
    /**
    case 'example_empty':
      return;
    // */
  }
}

/**
 * 公交换乘服务
 *
 * @return $form
 * @author cliu
 */
function bmap_bus_transfer($form, &$form_state) {
  $form['bmap_bus_transfer'] = array(
    '#type' => 'fieldset',
    '#title' => t('公交换乘'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['bmap_bus_transfer']['bmap_bus_transfer_origin'] = array(
    '#type' => 'textfield',
    // '#title' => t('出发点'),
    '#size' => 20,
    '#default_value' => variable_get('bmap_bus_transfer_origin',  t('出发地')),
    '#attributes' => array(
      'id' => 'bmap_bus_transfer_origin',
    ),
  );
  $form['bmap_bus_transfer']['bmap_bus_transfer_origin_hidden'] = array(
    '#type' => 'hidden',
    '#default_value' => variable_get('bmap_bus_transfer_origin_hidden'),
    '#attributes' => array(
      'id' => 'bmap_bus_transfer_origin_hidden',
    ),
  );
  $form['bmap_bus_transfer']['bmap_bus_transfer_destination'] = array(
    '#type' => 'textfield',
    // '#title' => t('目的地'),
    '#size' => 20,
    '#default_value' => variable_get('bmap_bus_transfer_destination',  t('目的地')),
    '#attributes' => array(
      'id' => 'bmap_bus_transfer_destination',
    ),
  );
  $form['bmap_bus_transfer']['bmap_bus_transfer_destination_hidden'] = array(
    '#type' => 'hidden',
    '#default_value' => variable_get('bmap_bus_transfer_destination_hidden'),
    '#attributes' => array(
      'id' => 'bmap_bus_transfer_destination_hidden',
    ),
  );

  $form['bmap_bus_transfer']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('查询'),
  );
  return $form;
}

/**
 * 公交换乘服务
 *
 * @return $form
 * @author cliu
 */
function bmap_bus_transfer_submit($form, &$form_state) {
  drupal_set_message(t('The form has been submitted. 出发点="@first", 目的地="@last"',
    array('@first' => $form_state['values']['bmap_bus_transfer_origin'], '@last' => $form_state['values']['bmap_bus_transfer_destination'])));
  variable_set('bmap_bus_transfer_origin',  $form_state['values']['bmap_bus_transfer_origin']);
  variable_set('bmap_bus_transfer_destination',  $form_state['values']['bmap_bus_transfer_destination']);
  // drupal_add_js(drupal_get_path('module', 'bmap') .'/bmap_type/amap/features/BusTranfer.js');
}

/**
 * 公交线路查询
 *
 * @return $form
 * @author cliu
 */
function bmap_bus_line($form, &$form_state) {
  $form['bmap_bus_line'] = array(
    '#type' => 'fieldset',
    '#title' => t('公交线路'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['bmap_bus_line']['bmap_bus_line_keyword'] = array(
    '#type' => 'textfield',
    // '#title' => t('公交线路名称'),
    '#size' => 20,
    '#default_value' => variable_get('bmap_bus_line_keyword',  t('公交线路名称')),
    '#attributes' => array(
      'id' => 'bmap_bus_line_keyword',
    ),
  );

  $form['bmap_bus_line']['bmap_bus_line_keyword_hidden'] = array(
    '#type' => 'hidden',
    '#default_value' => variable_get('bmap_bus_line_keyword_hidden'),
    '#attributes' => array(
      'id' => 'bmap_bus_line_keyword_hidden',
    ),
  );

  $form['bmap_bus_line']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('查询'),
  );
  return $form;
}

function bmap_bus_line_submit($form, &$form_state) {
  variable_set('bmap_bus_line_keyword',  $form_state['values']['bmap_bus_line_keyword']);
  drupal_add_js(drupal_get_path('module', 'bmap') .'/bmap_type/amap/features/AliMapBusLine.js');
}

/**
 * 公交站点查询
 *
 * @return $form
 * @author cliu
 */
function bmap_bus_station($form, &$form_state) {
  $form['bmap_bus_station'] = array(
    '#type' => 'fieldset',
    '#title' => t('公交站点'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['bmap_bus_station']['bmap_bus_station_keyword'] = array(
    '#type' => 'textfield',
    // '#title' => t('公交站点名称'),
    '#size' => 20,
    '#default_value' => variable_get('bmap_bus_station_keyword',  t('公交站点名称')),
  );

  $form['bmap_bus_station']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('查询'),
  );
  return $form;
}

/**
 * 驾车服务
 */
function bmap_driving($which_block) {
  switch ($which_block) {
    case 'bmap_tools':
      return variable_get('bmap_block_tools_string');
    /**
    case 'example_empty':
      return;
    // */
  }
}