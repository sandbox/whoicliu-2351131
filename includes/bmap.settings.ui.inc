<?php

/**
 * @file
 * BMap settings form.
 */
function bmap_admin_settings($form, &$form_state) {
  $form['initialization'] = array(
    '#type' => 'fieldset',
    '#title' => t('Baidu Map Initialize'),
  );

  if (!module_exists('keys_api') && !module_exists('keys')) {
    $form['initialization']['bmap_api_key'] = array(
      '#type' => 'textfield',
      '#default_value' => variable_get('bmap_api_key', ''),
      '#size' => 50,
      '#maxlength' => 255,
    );
  }
  else {
    $form['initialization']['bmap_api_key'] = array(
      '#type' => 'item',
      '#title' => t('Your Baidu maps API Version 2 key'),
      '#value' => t('Managed by <a href="@url">Keys</a>.', array('@url' => url('admin/settings/keys'))),
    );
  }

  $form['initialization']['bmap_api_key']['#title'] = t('Baidu Maps API Key');
  $form['initialization']['bmap_api_key']['#description'] = t('This key is currently not required by Baidu. Entering a Key will allow you to track map usage in Baidu. If you want to use an API key, you can get one at <a href="http://lbsyun.baidu.com/apiconsole/key?application=key">Baidu Map API website</a>.');

  return system_settings_form($form);
}

/**
 * BMap settings form validation.
 */
function bmap_admin_settings_validate($form, &$form_state) {
  $bmap_api_key = trim($form_state['values']['bmap_api_key']);

  // http://lbsyun.baidu.com/apiconsole/key?application=key
  if (strlen($bmap_api_key) != 24) {
    form_set_error(t('Baidu Map Key\'s length must be 24!'));
  }
}

/**
 * BMap settings form validation.
 */
function bmap_admin_settings_submit($form, &$form_state) {
  variable_set('bmap_api_key', trim($form_state['values']['bmap_api_key']));

  drupal_set_message(t('Baidu Map settings saved.'));
}
