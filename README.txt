
CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Installation
 * Other configuration
 * Credits
 
INTRODUCTION
------------
This module will allows you to use Baidu Maps API to render a map in Drupal 7.

INSTALLATION
------------

1. Copy the entire bmap project directory (not just the contents) to your 
   normal module directory (ie: sites/all/modules)
   
2. Enable the bmap module at ?q=admin/build/modules

3. Visit the Baidu Map settings page at ?q=admin/config/services/bmap
   You need a (free) Baidu Maps API Browser key. Here is how to get one:
   See http://developer.baidu.com/map/index.php for more details.
   
OTHER CONFIGURATION
-------------------
   
1. Bmap field settings
   TODO
2. Bmap field display settings
   TODO

CREDITS
-------
Developer and maintainer: Changtao Liu ( http://drupal.org/user/1414970 )